<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Blinking count</source>
        <translation>Liczba mrugnięć</translation>
    </message>
    <message>
        <source>This option doesn&apos;t affect events &apos;new chat&apos; and &apos;new message&apos;</source>
        <translation>To ustawienie nie dotyczy zdarzeń &apos;nowa rozmowa&apos; i &apos;nowa wiadomość&apos;</translation>
    </message>
    <message>
        <source>Blinking delay</source>
        <translation>Opóźnienie mrugania</translation>
    </message>
    <message>
        <source>LED</source>
        <translation>Dioda LED</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Powiadomienia</translation>
    </message>
    <message>
        <source>Defines blinking frequency of the LED (half of the period)</source>
        <translation>Określa częstotliwość migania diody LED (połowę okresu)</translation>
    </message>
    <message numerus="yes">
        <source>%n millisecond(s)</source>
        <translation>
            <numerusform>%n milisekunda</numerusform>
            <numerusform>%n milisekundy</numerusform>
            <numerusform>%n milisekund</numerusform>
        </translation>
    </message>
    <message>
        <source>Diode</source>
        <translation>Dioda</translation>
    </message>
    <message>
        <source>ScrollLock</source>
        <translation>ScrollLock</translation>
    </message>
    <message>
        <source>NumLock</source>
        <translation>NumLock</translation>
    </message>
    <message>
        <source>CapsLock</source>
        <translation>CapsLock</translation>
    </message>
</context>
</TS>
