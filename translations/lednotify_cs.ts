<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Notifications</source>
        <translation>Oznámení</translation>
    </message>
    <message>
        <source>LED</source>
        <translation>LED</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <source>Blinking count</source>
        <translation>Počet bliknutí</translation>
    </message>
    <message>
        <source>This option doesn&apos;t affect events &apos;new chat&apos; and &apos;new message&apos;</source>
        <translation>tato volba neovlivní události &apos;nový rozhovor&apos; a &apos;nová zpráva&apos;</translation>
    </message>
    <message>
        <source>Blinking delay</source>
        <translation>Zpoždění bliknutí</translation>
    </message>
    <message>
        <source>Defines blinking frequency of the LED (half of the period)</source>
        <translation>Určuje četnost blikání LED (polovina doby)</translation>
    </message>
    <message numerus="yes">
        <source>%n millisecond(s)</source>
        <translation>
            <numerusform>%n milisekunda</numerusform>
            <numerusform>%n milisekundy</numerusform>
            <numerusform>%n milisekund</numerusform>
        </translation>
    </message>
    <message>
        <source>Diode</source>
        <translation>Dioda</translation>
    </message>
    <message>
        <source>ScrollLock</source>
        <translation>ScrollLock</translation>
    </message>
    <message>
        <source>NumLock</source>
        <translation>NumLock</translation>
    </message>
    <message>
        <source>CapsLock</source>
        <translation>CapsLock</translation>
    </message>
</context>
</TS>
