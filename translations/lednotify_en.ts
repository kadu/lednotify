<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <source>LED</source>
        <translation>LED</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Blinking count</source>
        <translation>Blinking count</translation>
    </message>
    <message>
        <source>This option doesn&apos;t affect events &apos;new chat&apos; and &apos;new message&apos;</source>
        <translation>This option doesn&apos;t affect events &apos;new chat&apos; and &apos;new message&apos;</translation>
    </message>
    <message>
        <source>Blinking delay</source>
        <translation>Blinking delay</translation>
    </message>
    <message>
        <source>Defines blinking frequency of the LED (half of the period)</source>
        <translation>Defines blinking frequency of the LED (half of the period)</translation>
    </message>
    <message numerus="yes">
        <source>%n millisecond(s)</source>
        <translation>
            <numerusform>%n millisecond</numerusform>
            <numerusform>%n milliseconds</numerusform>
        </translation>
    </message>
    <message>
        <source>Diode</source>
        <translation>Diode</translation>
    </message>
    <message>
        <source>ScrollLock</source>
        <translation>ScrollLock</translation>
    </message>
    <message>
        <source>NumLock</source>
        <translation>NumLock</translation>
    </message>
    <message>
        <source>CapsLock</source>
        <translation>CapsLock</translation>
    </message>
</context>
</TS>
