/***************************************************************************
 *   Copyright (C) 2008-2014                                               *
 *     Michał Małek  michalm@jabster.pl                                    *
 *     Piotr Dąbrowski  ultr@ultr.pl                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/




#include "chat/chat-manager.h"
#include "configuration/configuration-file.h"
#include "core/core.h"
#include "gui/widgets/chat-widget/chat-widget-repository.h"
#include "gui/widgets/custom-input.h"
#include "message/unread-message-repository.h"
#include "misc/kadu-paths.h"
#include "misc/misc.h"
#include "notify/notification/aggregate-notification.h"
#include "notify/notification/new-message-notification.h"
#include "notify/notification-manager.h"
#include "activate.h"
#include "debug.h"
#include "exports.h"

#include "leddriver.h"

#include "lednotify.h"




bool LedNotify::init( bool firstLoad )
{
	Q_UNUSED( firstLoad );
	kdebugf();
	// register configuration
	MainConfigurationWindow::registerUiFile( KaduPaths::instance()->dataPath() + "plugins/configuration/lednotify.ui" );
	// init setup
	config_file.addVariable( "LedNotify", "LEDdiode", LedDriver::DiodeScrollLock );
	config_file.addVariable( "LedNotify", "LEDdelay",                        500 );
	config_file.addVariable( "LedNotify", "LEDcount",                          3 );
	NotificationManager::instance()->registerNotifier( this );
	connect( Core::instance()->unreadMessageRepository(), SIGNAL(unreadMessageRemoved(Message)) , this, SLOT(messageReceived(Message))       );
	connect( ChatManager::instance()                    , SIGNAL(chatUpdated(const Chat&))      , this, SLOT(chatUpdated(const Chat&))       );
	connect( Core::instance()->chatWidgetRepository()   , SIGNAL(chatWidgetRemoved(ChatWidget*)), this, SLOT(chatWidgetRemoved(ChatWidget*)) );
	// done
	kdebugf2();
	return true;
}


void LedNotify::done()
{
	kdebugf();
	disconnect( Core::instance()->unreadMessageRepository(), SIGNAL(unreadMessageRemoved(Message)) , this, SLOT(messageReceived(Message))       );
	disconnect( ChatManager::instance()                    , SIGNAL(chatUpdated(const Chat&))      , this, SLOT(chatUpdated(const Chat&))       );
	disconnect( Core::instance()->chatWidgetRepository()   , SIGNAL(chatWidgetRemoved(ChatWidget*)), this, SLOT(chatWidgetRemoved(ChatWidget*)) );
	// unregister configuration
	NotificationManager::instance()->unregisterNotifier( this );
	MainConfigurationWindow::unregisterUiFile( KaduPaths::instance()->dataPath() + "plugins/configuration/lednotify.ui" );
	// done
	kdebugf2();
}


LedNotify::LedNotify() :
	Notifier( "lednotify", QT_TRANSLATE_NOOP( "@default", "LED" ), KaduIcon( "kadu_icons/notify-led" ) ),
	chatBlinking_( false ), msgBlinking_( false )
{
}


LedNotify::~LedNotify()
{
}


NotifierConfigurationWidget* LedNotify::createConfigurationWidget( QWidget *widget )
{
	Q_UNUSED( widget );
	return 0;
}

#include <typeinfo>
void LedNotify::notify( Notification *notification )
{
	kdebugf();
	AggregateNotification *aggregatenotification = dynamic_cast<AggregateNotification*>( notification );
	if( aggregatenotification != nullptr )
	{
		foreach( Notification *notification, aggregatenotification->notifications() )
		{
			notify(notification);
		}
	}
	else
	{
		if( notification->type() == "NewChat" )
		{
			// Don't blink, if "OpenChatOnMessage" is "true" - chat is already open
			if( ! config_file.readBoolEntry( "Chat", "OpenChatOnMessage" ) )
			{
				chatBlinking_ = true;
				blinker_.startInfinite();
			}
		}
		else if( notification->type() == "NewMessage" )
		{
			MessageNotification *messagenotification = dynamic_cast<MessageNotification*>( notification );
			if( messagenotification != nullptr )
			{
				Chat chat = messagenotification->chat();
				ChatWidget* chatwidget = Core::instance()->chatWidgetRepository()->widgetForChat( chat );
				if( chatwidget != nullptr )
				{
					if( ! _isActiveWindow( chatwidget->window() ) )
					{
						msgChats_.insert( chat );
						msgBlinking_ = true;
						blinker_.startInfinite();
					}
					else if( ! config_file.readBoolEntry( "Notify", "NewMessageOnlyIfInactive" ) )
					{
						blinker_.startFinite();
					}
				}
			}
		}
		else
		{
			blinker_.startFinite();
		}
	}
	kdebugf2();
}


void LedNotify::messageReceived( Message message )
{
	Q_UNUSED( message );
	kdebugf();
	// Check if we can stop blinking from "NewChat" event...
	if(
		chatBlinking_ &&
		message.messageChat().unreadMessagesCount() == 0 &&
		( ! Core::instance()->unreadMessageRepository()->hasUnreadMessages() )
	)
	{
		chatBlinking_ = false;
		// ...and make sure "NewMessage" blinking is not running
		if( ! msgBlinking_ )
			blinker_.stop();
	}
	kdebugf2();
}


void LedNotify::chatUpdated( const Chat &chat )
{
	kdebugf();
	if( chat.unreadMessagesCount() == 0 )
	{
		chatRead( chat );
	}
	kdebugf2();
}


void LedNotify::chatWidgetRemoved( ChatWidget *chatwidget )
{
	kdebugf();
	chatRead( chatwidget->chat() );
	kdebugf2();
}


void LedNotify::chatRead( const Chat &chat )
{
	kdebugf();
	msgChats_.remove( chat );
	// Check if we can stop blinking from "NewMessage" event...
	if( msgBlinking_ && msgChats_.empty() )
	{
		msgBlinking_ = false;
		// ...and make sure "NewChat" blinking is not running
		if( ! chatBlinking_ )
			blinker_.stop();
	}
	kdebugf2();
}




#include "moc_lednotify.cpp"

Q_EXPORT_PLUGIN2( lednotify, LedNotify )
